# Roomba Director

Warning: This project is work in progress and highly unstable.

A small toolkit for Roomba vacuum cleaning robots.

## Goal

The goal of this project is to use the great features of the roomba robots, like cleaning maps and others without using the vendor cloud.

## Usage

First of all you need to activate the Roomba robot with the app.

Then you need to get the credentials of the robot. To do this first press the
home button of your Roomba until signal sounds (approximately 2 sec).

Then get credentials with `RoombaDirector`:

    RoombaDirector --get-credentials --hostname <Roomba's IP>

If you want to write the credentials to a file you can run:

    RoombaDirector --get-credentials --hostname <Roomba's IP> --credentials-file <filepath> --create-credentials-file

## Build

Clone repo and run

    go build

inside repo folder

