package client

import (
    "time"
    "crypto/tls"
    "encoding/json"

    "gitlab.com/JulianJacobi/RoombaDirector/credentials"

    MQTT "github.com/eclipse/paho.mqtt.golang"
)


func NewClient(creds credentials.Credentials, hostname string) (*Client, error) {
    defaultChannel := make(chan []byte)

    opts := MQTT.NewClientOptions()
    opts.AddBroker("ssl://"+hostname+":8883")
    opts.SetClientID(creds.Username)
    opts.SetUsername(creds.Username)
    opts.SetPassword(creds.Password)
    opts.SetTLSConfig(&tls.Config{InsecureSkipVerify: true})

    opts.SetDefaultPublishHandler(func (client MQTT.Client, msg MQTT.Message) {
        defaultChannel <- msg.Payload()
    })

    client := MQTT.NewClient(opts)
    connectToken := client.Connect()

    if connectToken.Wait() && connectToken.Error() != nil {
        return nil, connectToken.Error()
    }

    var roboState State
    roboClient := Client{
        State:         roboState,
        Hostname:      hostname,
        MQTTClient:    &client,
        brokerStopCh:  make(chan struct{}),
        brokerSubCh:   make(chan chan struct{}),
        brokerUnsubCh: make(chan chan struct{}),
        brokerPubCh:   make(chan struct{}),
    }

    go roboClient.startBroker()

    go updateClientState(&roboClient, defaultChannel)

    return &roboClient, nil
}

func (c *Client) startBroker() {
    listeners := map[chan struct{}]struct{}{}
    for {
        select {
        case <- c.brokerStopCh:
            return
        case msgChan := <-c.brokerSubCh:
            listeners[msgChan] = struct{}{}
        case msgChan := <-c.brokerUnsubCh:
            delete(listeners, msgChan)
        case msg := <-c.brokerPubCh:
            for msgChan := range listeners {
                select {
                case msgChan <- msg:
                default:
                }
            }
        }
    }
}

func (c *Client) Register() (chan struct{}) {
    msgChan := make(chan struct{}, 5)
    c.brokerSubCh <- msgChan
    return msgChan
}

func (c *Client) Unregister(msgChan chan struct{}) {
    c.brokerUnsubCh <- msgChan
}

func (c *Client) sendCommand(command string) (error) {
    message := make(map[string]interface{})
    message["command"]   = command
    message["time"]      = (time.Now()).Unix()
    message["initiator"] = "localApp"

    return c.sendMessage(message, "cmd")
}

func (c *Client) sendDelta(delta map[string]interface{}) (error) {
    message := make(map[string]interface{})
    message["state"] = delta

    return c.sendMessage(message, "delta")
}

func (c *Client) sendMessage(message map[string]interface{}, topic string) (error) {
    messageJson, err := json.Marshal(message)
    if err != nil {
        return err
    }
    publishToken := (*(c.MQTTClient)).Publish(topic, 0x00, false, messageJson)
    publishToken.Wait()
    if publishToken.Error() != nil {
        return publishToken.Error()
    }
    return nil
}

func (c *Client) Start() (error) {
    return c.sendCommand("start")
}

func (c *Client) Clean() (error) {
    return c.sendCommand("clean")
}

func (c *Client) Pause() (error) {
    return c.sendCommand("pause")
}

func (c *Client) Stop() (error) {
    return c.sendCommand("stop")
}

func (c *Client) Resume() (error) {
    return c.sendCommand("resume")
}

func (c *Client) Dock() (error) {
    return c.sendCommand("dock")
}

func (c *Client) Evac() (error) {
    return c.sendCommand("evac")
}

func (c *Client) Train() (error) {
    return c.sendCommand("train")
}

