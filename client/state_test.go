package client

import (
    "testing"
    "time"
)

func TestUpdateClient(t *testing.T) {
    initJson := `
    {"state": { "reported":
    {
  "audio": {
    "active": false
  },
  "batPct": 100,
  "batteryType": "lith",
  "bbchg": {
    "aborts": [
      0,
      0,
      0
    ],
    "nChgOk": 24,
    "nLithF": 0
  },
  "bbchg3": {
    "avgMin": 451,
    "estCap": 12311,
    "hOnDock": 2060,
    "nAvail": 134,
    "nDocks": 52,
    "nLithChrg": 31,
    "nNimhChrg": 0
  },
  "bbmssn": {
    "aCycleM": 14,
    "aMssnM": 19,
    "nMssn": 40,
    "nMssnC": 26,
    "nMssnF": 6,
    "nMssnOK": 8
  },
  "bbnav": {
    "aExpo": 138,
    "aGain": 29,
    "aMtrack": 98,
    "nGoodLmrks": 1
  },
  "bbpanic": {
    "panics": [
      8,
      8,
      8,
      8,
      8
    ]
  },
  "bbpause": {
    "pauses": [
      17,
      4,
      17,
      5,
      1,
      14,
      2,
      17,
      17,
      5
    ]
  },
  "bbrstinfo": {
    "causes": "0000",
    "nMobRst": 0,
    "nNavRst": 3
  },
  "bbrun": {
    "hr": 16,
    "min": 13,
    "nCBump": 0,
    "nCliffsF": 395,
    "nCliffsR": 435,
    "nMBStll": 3,
    "nPanics": 68,
    "nPicks": 236,
    "nScrubs": 45,
    "nStuck": 12,
    "nWStll": 4,
    "sqft": 58
  },
  "bbswitch": {
    "nBumper": 17760,
    "nClean": 63,
    "nDock": 52,
    "nDrops": 160,
    "nSpot": 30
  },
  "bbsys": {
    "hr": 2258,
    "min": 39
  },
  "bin": {
    "full": false,
    "present": true
  },
  "binPause": false,
  "bootloaderVer": "4042",
  "cap": {
    "binFullDetect": 1,
    "carperBoost": 0,
    "eco": 1,
    "edge": 1,
    "langOta": 1,
    "maps": 1,
    "multiPass": 2,
    "ota": 2,
    "pose": 1,
    "pp": 1,
    "svcConf": 1
  },
  "carperBoost": false,
  "cleanMissionStatus": {
    "cycle": "none",
    "error": 0,
    "expireM": 0,
    "initiator": "localApp",
    "mssnM": 0,
    "nMssn": 40,
    "notReady": 0,
    "phase": "charge",
    "rechrgM": 0,
    "sqft": 0
  },
  "cleanSchedule": {
    "cycle": [
      "none",
      "none",
      "none",
      "none",
      "none",
      "none",
      "none"
    ],
    "h": [
      0,
      0,
      0,
      0,
      0,
      0,
      0
    ],
    "M": [
      0,
      0,
      0,
      0,
      0,
      0,
      0
    ]
  },
  "cloudEnv": "prod",
  "country": "DE",
  "dock": {
    "known": false
  },
  "ecoCharge": false,
  "hardwareRev": 3,
  "langs": [
    {
      "en-UK": 0
    },
    {
      "de-DE": 1
    }
  ],
  "language": 1,
  "lastCommand": {
    "command": "dock",
    "initiator": "localApp",
    "time": "2020-04-17T19:01:46+02:00"
  },
  "localtimeoffset": 120,
  "mac": "c0:e4:34:06:fe:c7",
  "mapUploadAllowed": true,
  "mobilityVer": "5865",
  "name": "Robo",
  "navSwVer": "01.12.01#1",
  "netinfo": {
    "addr": "10.145.23.41",
    "bssid": "98:9b:cb:7b:81:4d",
    "dhcp": true,
    "dns1": "10.145.23.254",
    "dns2": "0.0.0.0",
    "gw": "10.145.23.254",
    "mask": "255.255.255.0",
    "sec": 4
  },
  "noAutpPasses": false,
  "noPP": false,
  "openOnly": false,
  "pose": {
    "point": {
      "x": 0,
      "Y": 0
    },
    "theta": 0
  },
  "schedHold": false,
  "signal": {
    "rssi": -33,
    "snr": 57
  },
  "sku": "R981040",
  "softwareVer": "v2.4.8-44",
  "soundVer": "58",
  "svcEndpoints": {
    "svcDeplId": "v011"
  },
  "timezone": "Europe/Berlin",
  "twoPass": false,
  "tz": {
    "events": [
      {
        "dt": "2020-03-01T18:00:00+01:00",
        "off": 60
      },
      {
        "dt": "2020-03-29T03:00:01+02:00",
        "off": 120
      },
      {
        "dt": "2020-10-25T02:00:01+01:00",
        "off": 60
      }
    ],
    "ver": 8
  },
  "uiSwVer": "4582",
  "umiVer": "6",
  "utctime": "2020-04-17T21:55:02+02:00",
  "vacHigh": false,
  "wifiSwVer": "20992",
  "wifistat": {
    "cloud": 7,
    "uap": false,
    "wifi": 1
  },
  "wlcfg": {
    "sec": 7,
    "ssid": "Asgard"
  }
}}}
    `

    cnl := make(chan []byte)

    c := Client{}

    go updateClientState(&c, cnl)

    cnl <- []byte(initJson)

    close(cnl)

    time.Sleep(time.Second)

    if c.State.Wlcfg.Ssid != "Asgard" || c.State.Audio.Active != false {
        t.Errorf("Initial JSON read failed.")
    }

    cnl = make(chan []byte)

    go updateClientState(&c, cnl)

    cnl <- []byte(`
{"state": { "reported":
    {
  "audio": {
    "audio": true
  }}}}
`)

    close(cnl)

    time.Sleep(time.Second)

    if c.State.Wlcfg.Ssid != "Asgard" || c.State.Audio.Active == false {
        t.Errorf("State Update failure")
    }
}

