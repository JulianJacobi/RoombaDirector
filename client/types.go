package client

import (
    "net"
    "time"

    MQTT "github.com/eclipse/paho.mqtt.golang"
)


type Client struct {
    Hostname      string
    MQTTClient    *MQTT.Client
    State         State
    brokerStopCh  chan struct{}
    brokerSubCh   chan chan struct{}
    brokerUnsubCh chan chan struct{}
    brokerPubCh   chan struct{}
}

type RawMessage struct {
    State RawState
}

type RawState struct {
    Reported JSONBytes
}

type JSONBytes []byte

type State struct {
    Audio              Audio              `json:"audio"`
    BatPct             int                `json:"batPct"`
    BatteryType        string             `json:"batteryType"`
    Bbchg              Bbchg              `json:"bbchg"`
    Bbchg3             Bbchg3             `json:"bbchg3"`
    Bbmssn             Bbmssn             `json:"bbmssn"`
    Bbnav              Bbnav              `json:"bbnav"`
    Bbpanic            Bbpanic            `json:"bbpanic"`
    Bbpause            Bbpause            `json:"bbpause"`
    Bbrstinfo          Bbrstinfo          `json:"bbrstinfo"`
    Bbrun              Bbrun              `json:"bbrun"`
    Bbswitch           Bbswitch           `json:"bbswitch"`
    Bbsys              Bbsys              `json:"bbsys"`
    Bin                Bin                `json:"bin"`
    BinPause           bool               `json:"binPause"`
    BootloaderVer      string             `json:"bootloaderVer"`
    Cap                Cap                `json:"cap"`
    CarpetBoost        bool               `json:"carperBoost"`
    CleanMissionStatus CleanMissionStatus `json:"cleanMissionStatus"`
    CleanSchedule      CleanSchedule      `json:"cleanSchedule"`
    CloudEnv           string             `json:"cloudEnv"`
    Country            string             `json:"country"`
    Dock               Dock               `json:"dock"`
    EcoCharge          bool               `json:"ecoCharge"`
    HardwareRev        int                `json:"hardwareRev"`
    Langs              [2]map[string]int  `json:"langs"`
    Language           int                `json:"language"`
    LastCommand        LastCommand        `json:"lastCommand"`
    Localtimeoffset    int                `json:"localtimeoffset"`
    Mac                string             `json:"mac"`
    MapUploadAllowed   bool               `json:"mapUploadAllowed"`
    MobilityVer        string             `json:"mobilityVer"`
    Name               string             `json:"name"`
    NavSwVer           string             `json:"navSwVer"`
    Netinfo            NetInfo            `json:"netinfo"`
    NoAutoPasses       bool               `json:"noAutpPasses"`
    NoPP               bool               `json:"noPP"`
    OpenOnly           bool               `json:"openOnly"`
    Pose               Pose               `json:"pose"`
    SchedHold          bool               `json:"schedHold"`
    Signal             Signal             `json:"signal"`
    Sku                string             `json:"sku"`
    SoftwareVer        string             `json:"softwareVer"`
    SoundVer           string             `json:"soundVer"`
    SvcEndpoints       SvcEndpoints       `json:"svcEndpoints"`
    Timezone           string             `json:"timezone"`
    TwoPass            bool               `json:"twoPass"`
    Tz                 Tz                 `json:"tz"`
    UiSwVer            string             `json:"uiSwVer"`
    UmiVer             string             `json:"umiVer"`
    Utctime            Time               `json:"utctime"`
    VacHigh            bool               `json:"vacHigh"`
    WifiSwVer          string             `json:"wifiSwVer"`
    Wifistat           Wifistat           `json:"wifistat"`
    Wlcfg              Wlcfg              `json:"wlcfg"`
}

type Audio struct {
    Active bool `json:"active"`
}

type Bbchg struct {
    Aborts [3]int `json:"aborts"`
    NChgOk int    `json:"nChgOk"`
    NLithF int    `json:"nLithF"`
}

type Bbchg3 struct {
    AvgMin    int `json:"avgMin"`
    EstCap    int `json:"estCap"`
    HOnDock   int `json:"hOnDock"`
    NAvail    int `json:"nAvail"`
    NDocks    int `json:"nDocks"`
    NLithChrg int `json:"nLithChrg"`
    NNimhChrg int `json:"nNimhChrg"`
}

type Bbmssn struct {
    ACycleM int `json:"aCycleM"`
    AMssnM  int `json:"aMssnM"`
    NMssn   int `json:"nMssn"`
    NMssnC  int `json:"nMssnC"`
    NMssnF  int `json:"nMssnF"`
    NMssnOK int `json:"nMssnOK"`
}

type Bbnav struct {
    AExpo      int `json:"aExpo"`
    AGain      int `json:"aGain"`
    AMtrack    int `json:"aMtrack"`
    NGoodLmrks int `json:"nGoodLmrks"`
}

type Bbpanic struct {
    Panics [5]int `json:"panics"`
}

type Bbpause struct {
    Pauses [10]int `json:"pauses"`
}

type Bbrstinfo struct {
    Causes  string `json:"causes"`
    NMobRst int    `json:"nMobRst"`
    NNavRst int    `json:"nNavRst"`
}

type Bbrun struct {
    Hr       int `json:"hr"`
    Min      int `json:"min"`
    NCBump   int `json:"nCBump"`
    NCliffsF int `json:"nCliffsF"`
    NCliffsR int `json:"nCliffsR"`
    NMBStll  int `json:"nMBStll"`
    NPanics  int `json:"nPanics"`
    NPicks   int `json:"nPicks"`
    NScrubs  int `json:"nScrubs"`
    NStuck   int `json:"nStuck"`
    NWStll   int `json:"nWStll"`
    Sqft     int `json:"sqft"`
}

type Bbswitch struct {
    NBumper int `json:"nBumper"`
    NClean  int `json:"nClean"`
    NDock   int `json:"nDock"`
    NDrops  int `json:"nDrops"`
    NSpot   int `json:"nSpot"`
}

type Bbsys struct {
    Hr  int `json:"hr"`
    Min int `json:"min"`
}

type Bin struct {
    Full    bool `json:"full"`
    Present bool `json:"present"`
}

type Cap struct {
    BinFullDetect int `json:"binFullDetect"`
    CarpetBoost   int `json:"carperBoost"`
    Eco           int `json:"eco"`
    Edge          int `json:"edge"`
    LangOta       int `json:"langOta"`
    Maps          int `json:"maps"`
    MultiPass     int `json:"multiPass"`
    Ota           int `json:"ota"`
    Pose          int `json:"pose"`
    Pp            int `json:"pp"`
    SvcConf       int `json:"svcConf"`
}

type CleanMissionStatus struct {
    Cycle     string `json:"cycle"`
    Error     int    `json:"error"`
    ExpireM   int    `json:"expireM"`
    Initiator string `json:"initiator"`
    MssnM     int    `json:"mssnM"`
    NMssn     int    `json:"nMssn"`
    NotReady  int    `json:"notReady"`
    Phase     string `json:"phase"`
    RechrgM   int    `json:"rechrgM"`
    Sqft      int    `json:"sqft"`
}

type CleanSchedule struct {
    Cycle [7]string `json:"cycle"`
    H     [7]int    `json:"h"`
    M     [7]int    `josn:"m"`
}

type Dock struct {
    Known bool `json:"known"`
}

type LastCommand struct {
    Command   string `json:"command"`
    Initiator string `json:"initiator"`
    Time      Time   `json:"time"`
}

type NetInfo struct {
    Addr  IP     `json:"addr"`
    Bssid string `json:"bssid"`
    Dhcp  bool   `json:"dhcp"`
    Dns1  IP     `json:"dns1"`
    Dns2  IP     `json:"dns2"`
    Gw    IP     `json:"gw"`
    Mask  IP     `json:"mask"`
    Sec   int    `json:"sec"`
}

type Pose struct {
    Point Point  `json:"point"`
    Theta int    `json:"theta"`
}

type Point struct {
    X int `json:"x"`
    Y int `josn:"y"`
}

type Signal struct {
    Rssi int `json:"rssi"`
    Snr  int `json:"snr"`
}

type SvcEndpoints struct {
    SvcDeplId string `json:"svcDeplId"`
}

type Tz struct {
    Events [3]Event `json:"events"`
    Ver    int      `json:"ver"`
}

type Event struct {
    Dt  Time `json:"dt"`
    Off int  `json:"off"`
}

type Wifistat struct {
    Cloud int  `json:"cloud"`
    Uap   bool `json:"uap"`
    Wifi  int  `json:"wifi"`
}

type Wlcfg struct {
    Sec  int       `json:"sec"`
    Ssid HexString `json:"ssid"`
}

type IP struct {
    net.IP
}

type HexString string

type Time struct {
    time.Time
}

