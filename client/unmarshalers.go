package client

import (
    "net"
    "time"

    "encoding/binary"
    "encoding/hex"
    "encoding/json"
)

func (s * JSONBytes) UnmarshalJSON(rawValue []byte) (error) {
    *s = JSONBytes(rawValue)
    return nil
}

func (ip *IP) UnmarshalJSON(rawValue []byte) (error) {
    var intValue int
    realIP := make(net.IP, 4)
    err := json.Unmarshal(rawValue, &intValue)
    if err != nil {
        return err
    }
    binary.BigEndian.PutUint32(realIP, uint32(intValue))
    ip.IP = realIP
    return nil
}

func (s *HexString) UnmarshalJSON(rawValue []byte) (error) {
    var strValue string
    err := json.Unmarshal(rawValue, &strValue)
    if err != nil {
        return err
    }
    bytes, err := hex.DecodeString(strValue)
    if err != nil {
        return err
    }
    *s = HexString(bytes)
    return nil

}

func (t *Time) UnmarshalJSON(rawValue []byte) (error) {
    var intValue int
    err := json.Unmarshal(rawValue, &intValue)
    if err != nil {
        return err
    }
    t.Time = time.Unix(int64(intValue), 0)
    return nil
}

