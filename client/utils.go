package client

import (
    "log"
    "encoding/json"

//    "github.com/prometheus/client_golang/prometheus"
)


func updateClientState(client *Client, channel chan []byte) {
    for {
        incomming := <-channel
        var rawMessage RawMessage
        err := json.Unmarshal(incomming, &rawMessage)
        if err != nil {
            log.Print("JSON unmarshal error: ", err)
        }
        err = json.Unmarshal(rawMessage.State.Reported, &client.State)
        if err != nil {
            log.Print("JSON unmarshal error: ", err)
        }
        client.brokerPubCh <- struct{}{}
    }
}

