package credentials

import (
    "fmt"
    "net"
    "bytes"
    "strings"
    "io/ioutil"
    "encoding/json"
    "crypto/tls"
)

const maxUDPBuffer = 1024
var roombaMagicToken = []byte{0xef, 0xcc, 0x3b, 0x29, 0x00}

// Get public Robot data via UDP
func getPublicRobotData(hostname string) (*RobotData, error) {
    laddr, err := net.ResolveUDPAddr("udp", ":5678")
    if err != nil {
        return nil, err
    }
    raddr, err := net.ResolveUDPAddr("udp", hostname+":5678")
    if err != nil {
        return nil, err
    }

    buffer := make([]byte, maxUDPBuffer)

    conn, err := net.DialUDP("udp", laddr, raddr)
    if err != nil {
        return nil, err
    }

    conn.Write([]byte("irobotmcs"))

    n, _, err := conn.ReadFromUDP(buffer)

    if err != nil {
        return nil, err
    }

    var robotData RobotData
    err = json.Unmarshal(buffer[:n], &robotData)
    if err != nil {
        return nil, err
    }

    return &robotData, nil
}

// Gives error to show if the Roomba is not in credentials sending state.
func getRobotModeError() (error) {
    return fmt.Errorf("The Robot seems to be not in credentials sending state. Please press the home button of your Roomba Robot until a signal sounds (aproximately 2 sec) and try again.")
}

// Get Roombas password via custom
// MQTT auth packets
func getRobotPassword(hostname string) (string, error) {
    conn, err :=tls.Dial("tcp", hostname+":8883", &tls.Config{InsecureSkipVerify: true})
    if err != nil {
        return "", err
    }

    packet_header := []byte{0xf0, byte(len(roombaMagicToken))}

    n, err := conn.Write(append(packet_header, roombaMagicToken...))
    if err != nil {
        return "", err
    }

    input_buffer := make([]byte, 200)

    n, err = conn.Read(input_buffer)
    if err != nil {
        return "", err
    }
    if n < 2 || input_buffer[0] != 0xf0 || input_buffer[1] == 0x00 {
        return "", getRobotModeError()
    }
    content_length := int(input_buffer[1])
    packet_buffer := bytes.NewBuffer([]byte{})
    if n > 2 {
        for _, b := range input_buffer[2:n] {
            err = packet_buffer.WriteByte(b)
            if err != nil {
                return "", err
            }
        }
    }
    for packet_buffer.Len() < content_length {
        n, err = conn.Read(input_buffer)
        if err != nil {
            return "", err
        }
        _, err = packet_buffer.Write(input_buffer[:n])
        if err != nil {
            return "", err
        }
    }
    complete_packet := packet_buffer.Bytes()
    password := complete_packet[len(roombaMagicToken):]
    if len(password) == 0 {
        return "", getRobotModeError()
    }

    return string(password), nil
}

// Gives Roomba's username and passsword,
// optionally write it to a file 
func GetCredentials(hostname string) (*RobotData, *Credentials, error) {
    robotData, err := getPublicRobotData(hostname)

    if err != nil {
        return nil, nil, err
    }

    password, err := getRobotPassword(hostname)

    if err != nil {
        return robotData, nil, err
    }

    username := strings.SplitN(robotData.Hostname, "-", -1)[1]

    credentials := Credentials{Username: username, Password: password}

    return robotData, &credentials, nil
}


// The Credentials.WriteToFile method writes Credentials to file
func (c Credentials) WriteToFile(path string) (error) {
    fileContent, err := json.Marshal(c)
    if err != nil {
        return err
    }
    return ioutil.WriteFile(path, fileContent, 0600)
}

func LoadFromFile(path string) (*Credentials, error) {
    content, err := ioutil.ReadFile(path)
    if err != nil {
        return nil, err
    }
    var creds Credentials
    err = json.Unmarshal(content, &creds)
    if err != nil {
        return nil, err
    }
    return &creds, nil
}

