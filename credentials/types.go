package credentials

// Robot capabilities represents the 
// capabilities of a Roomba Robot
type RobotCapabilities struct {
    Pose int
    Ota int
    MultiPass int
    CarpetBoost int
    Pp int
    BinFullDetect int
    LangOta int
    Maps int
    Edge int
    Eco int
    SvcConf int
}

// RoboData reprensents the public robot data
type RobotData struct {
    Ver string
    Hostname string
    Robotname string
    Ip string
    Mac string
    Sw string
    Sku string
    Nv int
    Proto string
    Cap RobotCapabilities
}

// Represents Roomba credentials
type Credentials struct {
    Username string
    Password string
}

