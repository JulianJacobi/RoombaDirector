module gitlab.com/JulianJacobi/RoombaDirector

go 1.14

require (
	github.com/GeertJohan/go.rice v1.0.0
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/gorilla/websocket v1.4.2
	github.com/prometheus/client_golang v1.5.1
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
)
