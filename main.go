package main

import (
    "fmt"
    "flag"
    "os"

    "gitlab.com/JulianJacobi/RoombaDirector/credentials"
    "gitlab.com/JulianJacobi/RoombaDirector/client"
    "gitlab.com/JulianJacobi/RoombaDirector/metrics"
    "gitlab.com/JulianJacobi/RoombaDirector/web"
)

// Represents Roomba's MQTT credentials
type Credentials struct {
    Username string
    Password string
}

const version string = "v0.1"

var (
    showVersion           = flag.Bool("version", false, "Show version information and exit.")
    getCredentialsFlag    = flag.Bool("get-credentials", false, "Get credentials from roomba and exit")
    createCredentialsFile = flag.Bool("create-credentials-file", false, "Create credentials file, only appropriate with --get-credetials")
    credentialsFile       = flag.String("credentials-file", "", "Path to credentials configuration file.")
    hostname              = flag.String("hostname", "", "Hostname or IP of Roomba roto")
    listenAddress         = flag.String("listen-address", ":8080", "Address HTTP interface listens to.")
)

func main() {
    flag.Parse()

    if *showVersion {
        printVersion()
        os.Exit(0)
    }

    if *hostname == "" {
        fmt.Println("Missing Roomba hostname")
        os.Exit(1)
    }

    if *getCredentialsFlag {
        getCredentials()
        os.Exit(0)
    }

    creds, err := credentials.LoadFromFile(*credentialsFile)
    if err != nil {
        fmt.Println("Error while loading credentials file:", err)
        os.Exit(1)
    }

    roboClient, err := client.NewClient(*creds, *hostname)
    if err != nil {
        fmt.Println("Roomba connection error:", err)
    }

    updateChannel := roboClient.Register()

    go func() {
        for _ = range updateChannel {
            metrics.SetMetrics(roboClient)
        }
    }()

    httpServer := web.NewServer(*listenAddress, roboClient)

    fmt.Println("WebGUI start listening on ", *listenAddress)

    fmt.Println(httpServer.ListenAndServe())
}

func printVersion() {
    fmt.Println("Roomba Director")
    fmt.Println("Verison:", version)
    fmt.Println("Author: Julian Jacobi <mail@julianjacobi.net>")
    fmt.Println("Toolkit to use great features of Roomba robots without vendor cloud.")
}

func getCredentials() {
    if *createCredentialsFile && *credentialsFile == "" {
        fmt.Println("If you want to create credentials file, you need to give --credentials-file")
    }
    robotData, creds, err := credentials.GetCredentials(*hostname)
    if robotData !=  nil {
        fmt.Println("Found Robot", robotData.Hostname)
        fmt.Println("Name:", robotData.Robotname)
        fmt.Println("IP:", robotData.Ip)
        fmt.Println("Model:", robotData.Sku)
        fmt.Println("Software-Version:", robotData.Sw)
        fmt.Println("")
    }
    if err != nil {
        fmt.Println(err)
        os.Exit(1)
    }
    fmt.Println("Username: ", creds.Username)
    fmt.Println("Password: ", creds.Password)

    if *createCredentialsFile {
        creds.WriteToFile(*credentialsFile)
    }
}

