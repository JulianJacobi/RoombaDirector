package metrics

import (
    "gitlab.com/JulianJacobi/RoombaDirector/client"

    "github.com/prometheus/client_golang/prometheus"
)

var (
    signalRssi = getGaugeVec(
        "signal_rssi",
        "Wifi signal strength",
    )
    signalSnr = getGaugeVec(
        "signal_snr",
        "Wifi signal noise ratio",
    )
    batteryPercentage = getGaugeVecWithLabels(
        "battery_percentage",
        "Battery percentage",
        []string{"battery_type"},
    )
    batteryFailures = getGaugeVecWithLabels(
        "battery_failures",
        "Battery failures",
        []string{"battery_type"},
    )
    batteryCycles = getGaugeVecWithLabels(
        "battery_cycles",
        "Battery cycles",
        []string{"battery_type"},
    )
    missionCount = getGaugeVecWithLabels(
        "mission_count",
        "Mission count",
        []string{"outcome"},
    )
    panicsCount = getGaugeVec(
        "panics_count",
        "Panics count",
    )
    picksCount = getGaugeVec(
        "picks_count",
        "Picks count",
    )
    scrubsCount = getGaugeVec(
        "scrubs_count",
        "Scrubs count",
    )
    stuckCount = getGaugeVec(
        "stuck_count",
        "Stuck count",
    )
    runSqft = getGaugeVec(
        "run_sqft",
        "Total run sqft",
    )
    runMinutes = getGaugeVec(
        "run_minutes",
        "Total run minutes",
    )
    switchPress = getGaugeVecWithLabels(
        "num_switch_pressed",
        "Number of switch presses",
        []string{"switch"},
    )
    systemAge = getGaugeVec(
        "system_age",
        "Minutes of system age",
    )
    binPresent = getGaugeVec(
        "bin_present",
        "Bin present",
    )
    binFull = getGaugeVec(
        "bin_full",
        "Bin full",
    )
    binPause = getGaugeVec(
        "bin_pause",
        "Bin pause",
    )
    carpetBoost = getGaugeVec(
        "carpet_boost",
        "Carpet boost",
    )
    missionError = getGaugeVec(
        "mission_error",
        "Mission error",
    )
    missionExpireM = getGaugeVec(
        "mission_expire_m",
        "Mission expire Minutes",
    )
    missionMinutes = getGaugeVec(
        "mission_minutes",
        "Mission minutes",
    )
    missionRechargeM = getGaugeVec(
        "mission_recharge_m",
        "Mission recharge minutes",
    )
    missionSqft = getGaugeVec(
        "mission_sqft",
        "Mission Squarefeet",
    )
    dockKnown = getGaugeVec(
        "dock_known",
        "Dock known",
    )
    ecoCharge = getGaugeVec(
        "eco_charge",
        "Eco charge",
    )
)


func init() {
    prometheus.MustRegister(signalRssi)
    prometheus.MustRegister(signalSnr)
    prometheus.MustRegister(batteryPercentage)
    prometheus.MustRegister(batteryFailures)
    prometheus.MustRegister(batteryCycles)
    prometheus.MustRegister(missionCount)
    prometheus.MustRegister(panicsCount)
    prometheus.MustRegister(picksCount)
    prometheus.MustRegister(scrubsCount)
    prometheus.MustRegister(stuckCount)
    prometheus.MustRegister(runSqft)
    prometheus.MustRegister(runMinutes)
    prometheus.MustRegister(switchPress)
    prometheus.MustRegister(systemAge)
    prometheus.MustRegister(binPresent)
    prometheus.MustRegister(binFull)
    prometheus.MustRegister(binPause)
    prometheus.MustRegister(carpetBoost)
    prometheus.MustRegister(missionError)
    prometheus.MustRegister(missionExpireM)
    prometheus.MustRegister(missionMinutes)
    prometheus.MustRegister(missionRechargeM)
    prometheus.MustRegister(missionSqft)
    prometheus.MustRegister(dockKnown)
    prometheus.MustRegister(ecoCharge)
}

func setGaugeForClient(gauge *prometheus.GaugeVec, c *client.Client, value int) {
    setGaugeForClientWithLabels(
        gauge,
        c,
        value,
        prometheus.Labels{},
    )
    gauge.With(prometheus.Labels{"robot": c.Hostname}).Set(float64(value))
}
func setGaugeForClientWithLabels(gauge *prometheus.GaugeVec, c *client.Client, value int, add_labels prometheus.Labels) {
    add_labels["robot"] = c.Hostname
    gauge.With(add_labels).Set(float64(value))
}

func SetMetrics(c *client.Client) {
    setGaugeForClient(signalRssi,        c, c.State.Signal.Rssi)
    setGaugeForClient(signalSnr,         c, c.State.Signal.Snr)
    if c.State.BatteryType != "" {
        setGaugeForClientWithLabels(
            batteryPercentage, c, c.State.BatPct,
            prometheus.Labels{"battery_type": c.State.BatteryType},
        )
    }
    setGaugeForClientWithLabels(
        batteryFailures,   c, c.State.Bbchg.NLithF,
        prometheus.Labels{"battery_type": "lith"},
    )
    setGaugeForClientWithLabels(
        batteryCycles,     c, c.State.Bbchg3.NLithChrg,
        prometheus.Labels{"battery_type": "lith"},
    )
    setGaugeForClientWithLabels(
        batteryCycles,     c, c.State.Bbchg3.NNimhChrg,
        prometheus.Labels{"battery_type": "nimh"},
    )
    setGaugeForClientWithLabels(
        missionCount,      c, c.State.Bbmssn.NMssn,
        prometheus.Labels{"outcome": "overall"},
    )
    setGaugeForClientWithLabels(
        missionCount,      c, c.State.Bbmssn.NMssnC,
        prometheus.Labels{"outcome": "canceled"},
    )
    setGaugeForClientWithLabels(
        missionCount,      c, c.State.Bbmssn.NMssnF,
        prometheus.Labels{"outcome": "failed"},
    )
    setGaugeForClientWithLabels(
        missionCount,      c, c.State.Bbmssn.NMssnOK,
        prometheus.Labels{"outcome": "ok"},
    )
    setGaugeForClient(panicsCount, c, c.State.Bbrun.NPanics)
    setGaugeForClient(picksCount,  c, c.State.Bbrun.NPicks)
    setGaugeForClient(scrubsCount, c, c.State.Bbrun.NScrubs)
    setGaugeForClient(stuckCount,  c, c.State.Bbrun.NStuck)
    setGaugeForClient(runSqft,     c, c.State.Bbrun.Sqft)
    setGaugeForClient(runMinutes,  c, (c.State.Bbrun.Hr * 60) + c.State.Bbrun.Min)
    setGaugeForClientWithLabels(
        switchPress, c, c.State.Bbswitch.NBumper,
        prometheus.Labels{"switch": "bumper"},
    )
    setGaugeForClientWithLabels(
        switchPress, c, c.State.Bbswitch.NClean,
        prometheus.Labels{"switch": "clean"},
    )
    setGaugeForClientWithLabels(
        switchPress, c, c.State.Bbswitch.NDock,
        prometheus.Labels{"switch": "dock"},
    )
    setGaugeForClientWithLabels(
        switchPress, c, c.State.Bbswitch.NDrops,
        prometheus.Labels{"switch": "drops"},
    )
    setGaugeForClientWithLabels(
        switchPress, c, c.State.Bbswitch.NSpot,
        prometheus.Labels{"switch": "spot"},
    )
    setGaugeForClient(systemAge,        c, (c.State.Bbsys.Hr * 60) + c.State.Bbsys.Min)
    setGaugeForClient(binPresent,       c, boolToInt(c.State.Bin.Present))
    setGaugeForClient(binFull,          c, boolToInt(c.State.Bin.Full))
    setGaugeForClient(binPause,         c, boolToInt(c.State.BinPause))
    setGaugeForClient(carpetBoost,      c, boolToInt(c.State.CarpetBoost))
    setGaugeForClient(missionError,     c, c.State.CleanMissionStatus.Error)
    setGaugeForClient(missionExpireM,   c, c.State.CleanMissionStatus.ExpireM)
    setGaugeForClient(missionMinutes,   c, c.State.CleanMissionStatus.MssnM)
    setGaugeForClient(missionRechargeM, c, c.State.CleanMissionStatus.RechrgM)
    setGaugeForClient(missionSqft,      c, c.State.CleanMissionStatus.Sqft)
    setGaugeForClient(dockKnown,        c, boolToInt(c.State.Dock.Known))
    setGaugeForClient(ecoCharge,        c, boolToInt(c.State.EcoCharge))
}

func getGaugeVecWithLabels(name, help string, extra_lables []string) (*prometheus.GaugeVec) {
    return prometheus.NewGaugeVec(prometheus.GaugeOpts{
        Name: name,
        Help: help,
    }, append([]string{"robot"}, extra_lables...))
}

func getGaugeVec(name, help string) (*prometheus.GaugeVec) {
    return getGaugeVecWithLabels(name, help, []string{})
}

func boolToInt(b bool) (int) {
    if b {
        return 1
    }
    return 0
}

