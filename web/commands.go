package web

import (
    "log"
    "net/http"

    "gitlab.com/JulianJacobi/RoombaDirector/client"
)


func commandsHandler(c *client.Client) (*http.ServeMux) {
    mux := http.NewServeMux()

    for _, cmd := range []string{"start", "clean", "stop", "dock", "pause", "resume"} {
        mux.HandleFunc("/" + cmd, commandHandler(c, cmd))
    }

    return mux
}

func commandHandler(c *client.Client, command string) (func (http.ResponseWriter, *http.Request)) {

    var clientCommand func()(error)

    switch command {
    case "start":
        clientCommand = c.Start
    case "clean":
        clientCommand = c.Clean
    case "stop":
        clientCommand = c.Stop
    case "dock":
        clientCommand = c.Dock
    case "pause":
        clientCommand = c.Pause
    case "resume":
        clientCommand = c.Resume
    default:
        log.Fatal("Unknown command: ", command)
    }

    return func (response http.ResponseWriter, request *http.Request) {
        if request.Method != "POST" {
            response.WriteHeader(405)
            response.Write([]byte("Only POST requests are allowed."))
            return
        }
        err := clientCommand()
        if err != nil {
            log.Print("Error while sending command to robot: ", err)
            response.WriteHeader(500)
            response.Write([]byte("Internal server error"))
        }
        response.Write([]byte("Command successfully sent to robot."))
    }
}

