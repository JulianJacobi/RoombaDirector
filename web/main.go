package web

import (
    "log"
    "encoding/json"
    "net/http"

    "gitlab.com/JulianJacobi/RoombaDirector/client"

    "github.com/prometheus/client_golang/prometheus/promhttp"
    "github.com/GeertJohan/go.rice"
    "github.com/gorilla/websocket"
)


var upgrader = websocket.Upgrader{
    ReadBufferSize: 1024,
    WriteBufferSize: 1024,
}


func NewServer(listenAddress string, c *client.Client) (*http.Server) {

    httpHandler := http.NewServeMux()

    httpHandler.Handle("/commands/", http.StripPrefix("/commands", commandsHandler(c)))

    httpHandler.Handle("/", http.FileServer(rice.MustFindBox("webroot").HTTPBox()))

    httpHandler.HandleFunc("/ws/status", func (response http.ResponseWriter, request *http.Request) {
        conn, err := upgrader.Upgrade(response, request, nil)
        if err != nil {
            log.Print("Websocket connection upgrade failed. ", err)
            response.WriteHeader(400)
            return
        }

        err = conn.WriteJSON(c.State)
        if err == nil {
            go func(cl *client.Client, ws *websocket.Conn){
                cnl := cl.Register()
                for _ = range cnl {
                    err := ws.WriteJSON(cl.State)
                    if err != nil {
                        break;
                    }
                }
                cl.Unregister(cnl)
            }(c, conn)
        }
    })

    httpHandler.HandleFunc("/json", func (response http.ResponseWriter, request *http.Request) {
        jsonState, err := json.MarshalIndent(c.State, "", "  ")
        if err != nil {
            response.WriteHeader(500)
            response.Write([]byte("JSON encode error"))
            log.Print("JSON marshal error", err)
        }
        code, err := response.Write(jsonState)
        if err != nil {
            log.Printf("Response write error. Code %d: %s", code, err)
        }
    })

    httpHandler.Handle("/metrics", promhttp.Handler())
    httpServer := &http.Server{
        Addr: listenAddress,
        Handler: httpHandler,
    }

    return httpServer
}

