'use strict';

const wsUrl       = (location.protocol.indexOf('s') == -1 ? "ws" : "wss") + "://" + location.host + "/ws/status"
const baseUrl     = location.protocol + "//" + location.host
const e           = React.createElement;
const Container   = ReactBootstrap.Container;
const ButtonGroup = ReactBootstrap.ButtonGroup;
const Button      = ReactBootstrap.Button;
const Row         = ReactBootstrap.Row;
const Col         = ReactBootstrap.Col;
const Card        = ReactBootstrap.Card;
const Table       = ReactBootstrap.Table;
const Spinner     = ReactBootstrap.Spinner;


class WifiStats extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    return (
      <Card>
        <Card.Header>
          <b>
            Network Information
          </b>
        </Card.Header>
        <table size="sm" class="card-body table table-sm mb-0">
          <tr>
            <td>Wifi RSSI</td>
            <td>{ this.props.signal.rssi } dBm</td>
          </tr>
          <tr>
            <td>Wifi SNR</td>
            <td>{ this.props.signal.snr } dB</td>
          </tr>
        </table>
      </Card>
    )
  }

}

class ControlButtons extends React.Component {

  requestCommand = (command) => {
    let commands = ["start", "stop", "clean", "dock", "pause", "resume"]
    if (commands.includes(command)) {
      return () => {
        jQuery.post(baseUrl + "/commands/" + command)
      }
    }

  }

  render () {
    if (this.props.missionStatus === {}) {
      console.log("Empty Mission status")
      return
    }

    let cleanButton  = ""
       ,dockButton   = ""
       ,stopButton   = ""
       ,pauseButton  = ""
       ,resumeButton = ""
    if (this.props.missionStatus.cycle === "none" && this.props.missionStatus.notReady === 0) {
      cleanButton = (
        <Button variant="success" block onClick={ this.requestCommand("clean") }>
          Clean
        </Button>
      )
    }
    if (this.props.missionStatus.phase !== "charge") {
      dockButton = (
        <Button variant="danger" block onClick={ this.requestCommand("dock") }>
          Back to dock
        </Button>
      )
    }
    if (["run", "hmUsrDock"].includes(this.props.missionStatus.phase)) {
      stopButton = (
        <Button variant="danger" block onClick={ this.requestCommand("stop") }>
          Stop
        </Button>
      )
    }
    if (this.props.missionStatus.phase === "run") {
      pauseButton = (
        <Button variant="warning" block onClick={ this.requestCommand("pause") }>
          Pause
        </Button>
      )
    }
    if ( (!["none", "dock"].includes(this.props.missionStatus.cycle)) && this.props.missionStatus.phase === "stop") {
      resumeButton = (
        <Button variant="success" block onClick={ this.requestCommand("resume") }>
          Resume
        </Button>
      )
    }

    return (
      <div>
      { cleanButton }
      { resumeButton }
      { pauseButton }
      { stopButton }
      { dockButton }
      </div>
    )
  }
}


class AppBase extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      isLoaded: false,
      roboState: {},
    };
    
    this.ws = new WebSocket(wsUrl);
  }

  componentDidMount () {
    this.ws.onmessage = (e) => {
      var msg = JSON.parse(e.data)
      console.log(msg)
      this.setState({roboState: msg, isLoaded: true})
    }
  }
    
  render () {
    let wifi,
        missionStatus = {}
    if (this.state.isLoaded) {
      wifi = <WifiStats signal={ this.state.roboState.signal }/>
      missionStatus = this.state.roboState.cleanMissionStatus;
    } 
    else {
      wifi = "Test"
    }


    return (
      <Container>
        <Row className="justify-content-sm-center">
          <Col xs={12} sm={7} md={5} lg={4} xl={3} className="mt-3">
            <ControlButtons missionStatus={ missionStatus } />
            <h1>Status</h1>
            { wifi }
          </Col>
        </Row>
      </Container>
    )
  }
}

const domContainer = document.querySelector("#pseudobody");
ReactDOM.render(e(AppBase), domContainer)

